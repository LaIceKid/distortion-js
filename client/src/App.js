import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";


import IndexPage from "./Routers/index.jsx";
import RegistrationPage from "./Routers/registration.jsx";
import AuthentificationPage from "./Routers/authentification.jsx";
import Header from "./Components/header";




import SideBar from "./StaticHtml/sidebar.jsx";
import Footer from "./StaticHtml/footer.jsx";

import MapPage from "./Routers/map.jsx";




class App extends Component{
  render() {
    return (
        <Router>
          <Switch>
              <React.Fragment>
                  <Header/>
                  <Route exact path="/" component={IndexPage}/>
                  <Route exact path="/registration" component={RegistrationPage}/>
                  <Route exact path="/authentification" component={AuthentificationPage}/>

              </React.Fragment>

              <React.Fragment>



                  {/*
                  <Header/>
                  <div className="row character-side" style={{margin: "auto"}}>


                      <div className="sidebar col-md-4">
                        <SideBar/>
                      </div>
                      <div className="main col-md-8">
                          <Route path="/" exact component={IndexPage} />
                          <Route path="/map" exact component={MapPage} />
                      </div>


                  </div>

                  <Footer/>
                  */}
              </React.Fragment>
          </Switch>
        </Router>
    );
  }
}

export default App;