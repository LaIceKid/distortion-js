import React, {Component} from "react";
import { NavLink} from "react-router-dom";

class index extends Component{
    render() {
        return(
            <div>
                <div>
                    <NavLink to="/" > Home </NavLink>
                    <NavLink to="#" > about </NavLink>
                    <NavLink to="#" > history </NavLink>
                    <NavLink to="#" > news </NavLink>
                    <NavLink to="registration" > Registration </NavLink>
                    <NavLink to="authentification" > Authentification </NavLink>
                </div>
            </div>
        );
    }
}

export default index;