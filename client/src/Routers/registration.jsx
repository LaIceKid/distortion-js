import React, {Component} from "react";


class registration extends Component{

    constructor(){
        super()
        this.state ={
            email: '',
            password : '',
            preloader : false,
            succesMessage : false
        }

    }

    handleChange = name => event => {
        console.log(name, event.target.value)

        this.setState({[name]: event.target.value});
    }



    registration = e => {

        e.preventDefault()
        this.setState({
            preloader : true
        })


        const {email, password} = this.state

        const regForm = {
            email,
            password
        }

        fetch("http://localhost:3001/api/auth/registration", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(regForm)
        })
            .then(response => response.json())
            .then(data=>{
                if (data.message === 'success') {
                    this.setState({
                        preloader : false,
                        succesMessage : true
                    })
                }

                console.log('THIS IS DATA', data)
            })
            .catch (error => console.log(error))


    }


    render() {
        return(
            <div>
                <div>
                 <h1 style={{color:'white'}}> ME VAR REGISTRATION PAGE</h1>
                    <form>
                        <label>Emaili</label><br/>
                        <input type="text" name="email"
                               value={this.email}
                               onChange={this.handleChange('email')}

                        />
                        <br/>
                        <br/>
                        <label>Password</label><br/>
                        <input type="password" name="password"
                               value={this.password}
                               onChange={this.handleChange('password')}
                        />
                        <br/>
                        <br/>


                        {
                            this.state.preloader
                            ?
                                <>
                                    <h1> Loading....</h1>
                                </>
                                :
                                <>
                                    {
                                        this.state.succesMessage
                                            ?
                                            <>
                                                <div className="alert alert-success" > Registration Success</div>
                                            </>
                                            :
                                            <>
                                                <button onClick={this.registration.bind(this)}>Registration</button>
                                            </>
                                    }

                                </>
                        }



                    </form>


                </div>
            </div>
        );
    }
}

export default registration;