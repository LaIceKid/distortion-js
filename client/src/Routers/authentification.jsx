import React, {Component} from "react";


class authentification extends Component{

    constructor(){
        super()
        this.state ={
            email: '',
            password : '',
            preloader : false,
            succesMessage : false
        }

    }

    handleChange = name => event => {
        console.log(name, event.target.value)

        this.setState({[name]: event.target.value});
    }



    authentification = e => {

        e.preventDefault()
        this.setState({
            preloader : true
        })


        const {email, password} = this.state

        const authForm = {
            email,
            password
        }

        fetch("http://localhost:3001/api/auth/authentification", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(authForm)
        })
            .then(response => response.json())
            .then(data=>{
                if (data.message === 'success') {
                    this.setState({
                        preloader : false,
                        succesMessage : true
                    })
                }

                console.log('THIS IS DATA', data)
            })
            .catch (error => console.log(error))


    }


    render() {
        return(
            <div>
                <div>
                    <h1 style={{color:'white'}}> ME VAR AUTHENTIFICATION PAGE</h1>
                    <form>
                        <label>Emaili</label><br/>
                        <input type="text" name="email"
                               value={this.email}
                               onChange={this.handleChange('email')}

                        />
                        <br/>
                        <br/>
                        <label>Password</label><br/>
                        <input type="password" name="password"
                               value={this.password}
                               onChange={this.handleChange('password')}
                        />
                        <br/>
                        <br/>


                        {
                            this.state.preloader
                                ?
                                <>
                                    <h1> Loading....</h1>
                                </>
                                :
                                <>
                                    {
                                        this.state.succesMessage
                                            ?
                                            <>
                                                <div className="alert alert-success" > Authentification Success</div>
                                            </>
                                            :
                                            <>
                                                <button onClick={this.authentification.bind(this)}>Authentification</button>
                                            </>
                                    }

                                </>
                        }



                    </form>


                </div>
            </div>
        );
    }
}

export default authentification;