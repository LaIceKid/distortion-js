import React, {Component} from "react";
import App
    from "../App";
class footer extends Component{
    render() {
        return(
            <footer className="footer">
                <div className="container">
                    <span className="text-muted">Copyright 2019 &copy; All Rights Reserved</span>
                </div>
            </footer>
        );
    }
}

export default footer;