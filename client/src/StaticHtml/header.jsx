import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import App
    from "../App";

class header extends Component{
    render() {
        return(
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <NavLink className="navbar-brand" href="#">
                            <img src="http://127.0.0.1/distortion/assets/images/logo.svg" alt="" width="20" />
                            Distortion
                        </NavLink>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon" />
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <NavLink className="nav-link" to="/">HIVE <span className="sr-only">(current)</span></NavLink>
                                </li>
                                <li className="nav-item active">
                                    <NavLink className="nav-link" to="map">MAPS</NavLink>
                                </li>
                                {/*<li className="nav-item dropdown">*/}
                                {/*    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">*/}
                                {/*        MAPS*/}
                                {/*    </a>*/}
                                {/*    <div className="dropdown-menu" aria-labelledby="navbarDropdown">*/}
                                {/*        <NavLink className="dropdown-item" to="map">Swamp</NavLink>*/}
                                {/*        <a className="dropdown-item disabled" href="#">New Yucky</a>*/}
                                {/*    </div>*/}
                                {/*</li>*/}
                                <li className="nav-item">
                                    <NavLink className="nav-link" to="/">INVENTORY</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link disabled" to="/">STORAGE</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link disabled" to="/">ARMORY</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link disabled" to="/">CRAFTING</NavLink>
                                </li>
                            </ul>
                        </div>
                    </nav>
            </div>
        );
    }
}

export default header;