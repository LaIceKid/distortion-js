const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const port = 3001
const app = express()
app.use(cors())
app.use(morgan("dev"))
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

app.get('/', (req, res) => res.send('lasha'))

mongoose.connect("mongodb://localhost:27017/distortion")
    .then(() => console.log('MongoDB connection success'))
    .catch(error => console.log(error))

module.exports = app;



//CONTROLLERS
const userRoutes = require('./routes/auth');

//ROUTES
app.use('/api/auth', userRoutes);