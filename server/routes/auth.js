const express = require('express');
const passport = require('passport')

const controller = require('../controllers/auth');
const router = express.Router();


router.post('/registration',  controller.registration)
router.post('/authentification',  controller.authentification)

/*

router.post('/login', controller.login)

router.post('/checkAuth',  controller.checkAuth)
router.post('/userInformation', controller.userInformation)

*/


module.exports = router;