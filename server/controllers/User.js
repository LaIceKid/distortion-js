const User = require('../models/User')

module.exports.register = async (request, response) => {
    try{
        let password = request.body.password;
        let email = request.body.email;

        let user = new User({
            email: email,
            password: password
        });

        await user.save();
        response.status(201).json({
            message:"success",
            error:"error",
            user
        });
    }catch(e){
        console.log('Error', e)
    }
}