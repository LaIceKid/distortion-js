const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')


const User = require('../models/User');

module.exports.authentification = async (req, res) => {
    let candidate = await User.findOne({email:req.body.email})
    res.status(201).json({
        candidate,
        email: req.body.email,
        password: req.body.email,
        message: 'success'
    })
}

module.exports.registration = async (req, res) => {
    try {
        let email = req.body.email;

        let candidate = await User.findOne({email:email})

        if (!candidate) {
            let salt = bcrypt.genSaltSync(10)
            let password = req.body.password;

            let user = new User ({
                email : email,
                salt: salt,
                password: bcrypt.hashSync(password, salt)

            })

            await user.save(function (err, user) {
                if (err) {
                    console.log(err);
                }
                else {
                    res.status(201).json({
                        user,
                        message: 'success'
                    })

                }
            })


            /*
                        email:{
                            type: String,
                                required:true,
                                unique:true
                        },
                        salt:{
                            type:String
                        },
                        password:{
                            type:String
                        },
                        status:{
                            type:Number,
                        default:1
                        },

                        */

        }else {

        }

    }catch (e) {
        console.log('error',e)
    }

}