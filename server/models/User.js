const mongoose = require ('mongoose')
const Schema = mongoose.Schema

const User = new Schema ({
    email:{
        type: String,
        required:true,
        unique:true
    },
    salt:{
        type:String
    },
    password:{
        type:String
    },
    status:{
        type:Number,
        default:1
    },
    actionDate:{
        type:Date,
        default: new Date()
    },
    createdAt: {
        type:Date,
        default:new Date()
    }
})

module.exports = mongoose.model('User', User)