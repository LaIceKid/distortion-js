module.exports = {
    _URL_CLIENT: 'http://localhost:3000',
    _URL_SERVER: 'http://localhost:3001',
    _MONGO_URI : "mongodb://localhost:27017/hookupoBD",
    _PORT : 8080,
    _JWT_SECRET : "asyncdistortionworld",
    _TEST :   new Date().getTime(),
    _COMMENTARY_COUNT_LIMIT_SHOW :   3,
    _TIMELINE_COUNT_LIMIT_SHOW :   2,
    _PHOTOS_COUNT_LIMIT_SHOW :   8,
    _USERS_COUNT_LIMIT_SHOW :   16,
    _LIMITES :   8,

}
